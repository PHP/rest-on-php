<?php
namespace RestOnPhp\Session;

use Lcobucci\JWT\Token;
use Lcobucci\JWT\Token\Plain as PlainToken;
use RestOnPhp\Security\SecureUser;

class JwtSessionStorage {
    private $user;
    private $token;

    /**
     * @return SecureUser
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @return PlainToken
     */
    public function getToken() {
        return $this->token;
    }

    /**
     * @param SecureUser
     */
    public function setUser(SecureUser $user) {
        $this->user = $user;
    }

    public function setToken(Token $token) {
        $this->token = $token;
    }
}
