<?php
namespace RestOnPhp\Handler;

use Doctrine\ORM\EntityManager;
use RestOnPhp\Metadata\XmlMetadata;
use RestOnPhp\Event\ResourcePreDeleteEvent;
use RestOnPhp\Repository\DefaultRepository;
use RestOnPhp\Event\ResourcePostDeleteEvent;
use Symfony\Component\HttpFoundation\Response;
use RestOnPhp\Handler\Response\HandlerResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

class CollectionHandler implements HandlerInterface {
    use PaginationTrait;

    private $filters;
    private $request;
    private $metadata;
    private $dispatcher;
    private $entityManager;
    private $requestStack;

    public function __construct(
        EventDispatcher $dispatcher, 
        EntityManager $entityManager, 
        XmlMetadata $metadata, 
        RequestStack $requestStack,
    ) {
        
        $this->filters = [];
        $this->metadata = $metadata;
        $this->dispatcher = $dispatcher;
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

    public function handle($resource_name) {
        $this->request = $this->requestStack->getCurrentRequest();

        $metadata = $this->metadata->getMetadataFor($resource_name);
        $id_field = $this->metadata->getIdFieldNameFor($resource_name);

        $doctrine_metadata = $this->entityManager->getClassMetadata($metadata['entity']);
        $f = $this->request->query->all('filter') ? $this->request->query->all('filter') : [];

        $filters = [
            'exact' => [],
            'partial' => [],
            'lt' => [],
            'gt' => [],
            'lte' => [],
            'gte' => [],
            'relation' => [],
            'default' => $this->filters
        ];
    

        foreach($f as $field => $filter) {
            if(in_array($field, ['gt', 'lt', 'gte', 'lte', 'default'])) {
                $filters[$field] = $filter;
                continue;
            }

            $hasField = $doctrine_metadata->hasField($field);
            $hasAssociation = $doctrine_metadata->hasAssociation($field);

            if(!$hasField && !$hasAssociation) {
                continue;
            }

            $field_metadata = $this->metadata->getFieldMetadataFor($resource_name, $field);
            
            if(null === $field_metadata) {
                continue;
            }

            $filter_type = isset($field_metadata['filter-type']) ? $field_metadata['filter-type'] : 'exact';
            $filters[$filter_type][$field] = [
                'value' => $filter,
                'metadata' => $field_metadata
            ];
        }

        $pagination_parameters = $this->getPaginationParameters();

        $order_param = $this->request->query->all('order') ? $this->request->query->all('order') : [
            $id_field => 'ASC'
        ];

        $order = [];

        foreach($order_param as $order_field => $order_direction) {
            if(!$doctrine_metadata->hasField($order_field)) {
                unset($order[$order_field]);
            } else {
                $order[$order_field] = $order_direction;
            }
        }

        $entityMetadata = $this->entityManager->getClassMetadata($metadata['entity']);

        if(!$entityMetadata->customRepositoryClassName) {
            $entityMetadata->setCustomRepositoryClass(DefaultRepository::class);
        }

        /**
         * @var \Doctrine\ORM\EntityRepository $entityRepository
         */
        $entityRepository = $this->entityManager->getRepository($metadata['entity']);
        $paginator = $entityRepository->get($filters, $pagination_parameters, $order);
        $pagination = $this->pagination($resource_name, $paginator, $pagination_parameters);
        return new HandlerResponse(HandlerResponse::CARDINALITY_COLLECTION, $paginator, $pagination);
    }

    public function bulkDelete($resource_name, $ids) {
        $metadata = $this->metadata->getMetadataFor($resource_name);
        $ids = explode(',', $ids);

        if(!is_array($ids)) {
            throw new NotAcceptableHttpException('Action only accepts a list of IDs');
        }

        array_walk($ids, 'trim');
        $id_field = $this->metadata->getIdFieldNameFor($resource_name);

        $entityRepository = $this->entityManager->getRepository($metadata['entity']);

        $data = $entityRepository->get([ 
            'partial' => [], 
            'exact' => [ $id_field => [ 'value' => $ids, 'metadata' => [] ] ],
            'relation' => [],
            'lte' => [],
            'gte' => [],
            'lt' => [],
            'gt' => [],
            'default' => $this->filters
        ], false, [], false);

        foreach($data as $record) {
            $this->dispatcher->dispatch(new ResourcePreDeleteEvent($record), ResourcePreDeleteEvent::NAME);
            $this->entityManager->remove($record);
            $this->dispatcher->dispatch(new ResourcePostDeleteEvent($record), ResourcePostDeleteEvent::NAME);
        }

        $this->entityManager->flush();
        return new HandlerResponse(HandlerResponse::CARDINALITY_NONE, new Response('', 204));
    }

    public function setFilters($filters) {
        $this->filters = $filters;
    }

    public function setFillers($fillers) {
    }
}
