<?php

namespace RestOnPhp\Handler\Response;

class HandlerResponse implements HandlerResponseInterface {

    private $data;
    private $extras;
    private $pagination;
    private $cardinality;

    public function __construct(
        int $cardinality, 
        $data, 
        $pagination = null, 
        $extras = []
    ) {
        $this->data = $data;
        $this->extras = $extras;
        $this->pagination = $pagination;
        $this->cardinality = $cardinality;
    }

    function getCardinality() {
        return $this->cardinality;
    }

    function getData() {
        return $this->data;
    }

    function getPagination() {
        return $this->pagination;
    }

    function getExtras() {
        return $this->extras;
    }
}