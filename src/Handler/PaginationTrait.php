<?php

namespace RestOnPhp\Handler;

use Exception;

trait PaginationTrait {
    protected function getPaginationParameters() {
        $pagination_parameters = null;

        try {
            $pagination_parameters = $this->request->query->all('pagination');
        } catch(Exception $e) {
            $pagination_parameters = $this->request->query->get('pagination');
        }
        
        if(null == $pagination_parameters) {
            $pagination_parameters = [
                'page' => 1,
                'per_page' => 10
            ];
        } else if('0' == $pagination_parameters || 'false' == $pagination_parameters) {
            $pagination_parameters = false;
        } else {
            if(!isset($pagination_parameters['page'])) {
                $pagination_parameters['page'] = 1;
            }

            if(!isset($pagination_parameters['per_page'])) {
                $pagination_parameters['per_page'] = 10;
            }
        }

        return $pagination_parameters;
    }

    protected function pagination(
        $resource_name, 
        $paginator, 
        $pagination_parameters,
        $routeName = 'getCollection'
    ) {

        if($pagination_parameters == false) {
            return [];
        }

        $total_items = count($paginator);
        $route = $this->metadata->getRouteMetadataFor($resource_name, $routeName);
        $params = $this->request->query->all();
        $params['pagination']['page'] = $pagination_parameters['page'];
        $params['pagination']['per_page'] = $pagination_parameters['per_page'];

        $current_page = $route['path'] . '?' . http_build_query($params);
        $params['pagination']['page'] = $pagination_parameters['page'] - 1;
        $previous_page = $route['path'] . '?' . http_build_query($params);
        $params['pagination']['page'] = $pagination_parameters['page'] + 1;
        $next_page = $route['path'] . '?' . http_build_query($params);

        if($pagination_parameters['per_page'] > 0) {
            $total_pages = ceil($total_items / $pagination_parameters['per_page']);
        } else {
            $total_pages = 0;
        }

        $pagination = [
        ];

        if($pagination_parameters['page'] - 1 > 0) {
            $pagination['previous_page'] = $previous_page;
        }

        $pagination['current_page'] = $current_page;

        if($pagination_parameters['page'] + 1 <= $total_pages) {
            $pagination['next_page'] = $next_page;
        }

        
        $pagination['total_pages'] = $total_pages;
        $pagination['total_items'] = $total_items;
        return $pagination;
    }
}