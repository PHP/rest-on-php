<?php
namespace RestOnPhp\Normalizer;

use Doctrine\ORM\EntityManager;
use RestOnPhp\Metadata\XmlMetadata;

/**
 * @property EntityManager $entityManager
 * @property XmlMetadata $xmlMetadata
 */
class CollectionIdsNormalizer {
    private $entityManager, $xmlMetadata;

    public function __construct(EntityManager $entityManager, XmlMetadata $xmlMetadata) {
        $this->entityManager = $entityManager;
        $this->xmlMetadata = $xmlMetadata;
    }

    public function denormalize($field, $value, $resource_metadata) {
        $id_field = $resource_metadata['id'];
        
        if(!$value) {
            return null;
        }

        $collection = [];

        foreach($value as $item) {
            $collection[] =  $this->entityManager->getRepository($resource_metadata['entity'])->findOneBy([
                $id_field => $item
            ]);
        }

        return $collection;
    }

    public function normalize($field, $data, $resource_metadata, $object) {
        if(!$data) {
            return null;
        }

        $collection = [];

        foreach($data as $item) {
            $id = $resource_metadata['id'];
            $getter = 'get' . ucfirst($id);
            $collection[] = $item->$getter();
        }

        return $collection;
    }
}