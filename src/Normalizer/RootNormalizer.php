<?php
namespace RestOnPhp\Normalizer;

use DateTime;
use RestOnPhp\Metadata\XmlMetadata;

class RootNormalizer {
    private $xmlMetadata, $normalizers;

    public function __construct(
        XmlMetadata $xmlMetadata,
        $normalizers = []
    ) {
        $this->normalizers = [];
        $this->xmlMetadata = $xmlMetadata;

        foreach($normalizers as $normalizer) {
            $this->normalizers[get_class($normalizer)] = $normalizer;
        }
    }

    public function denormalizeItem($data, $resource_metadata, $object = null) {
        if(!$object) {
            $object = new $resource_metadata['entity']();
        }
        
        foreach($data as $data_field => $value) {
            if(!isset($resource_metadata['fields'][$data_field])) {
                continue;
            }

            $field = $resource_metadata['fields'][$data_field];
            
            if(true == $field['readonly']) {
                continue;
            }

            $value = $data[$data_field];
            $entity_metadata = null;

            if(isset($field['normalizer'])) {
                if('relation_single' == $field['type']) {
                    $entity_metadata = $this->xmlMetadata->getMetadataFor($field['resource']);
                }

                if('relation_collection' == $field['type']) {
                    $entity_metadata = $this->xmlMetadata->getMetadataFor($field['resource']);
                }

                $value = $this->normalizers[$field['normalizer']]->denormalize(
                    $field,
                    $value, 
                    $entity_metadata,
                    $object
                );
            } else if('relation_single' == $field['type']) {
                $entity_metadata = $this->xmlMetadata->getMetadataFor($field['resource']);

                $value = $this->normalizers['RestOnPhp\Normalizer\ObjectIdNormalizer']->denormalize(
                    $field,
                    $value, 
                    $entity_metadata,
                    $object
                );
            } else if('relation_collection' == $field['type']) {
                $entity_metadata = $this->xmlMetadata->getMetadataFor($field['resource']);

                $value = $this->normalizers['RestOnPhp\Normalizer\CollectionIdsNormalizer']->denormalize(
                    $field,
                    $value, 
                    $entity_metadata,
                    $object
                );
            } else if('datetime' == $field['type']) {
                $value = DateTime::createFromFormat($field['datetime-format'], $value);

                if(!$value) {
                    $value = null;
                }
            }

            $this->invokeSetter($object, $data_field, $value);
        }
        
        return $object;
    }

    private function invokeSetter($object, $field, $value) {
        $setter = 'set' . ucfirst($field);
            
        if(method_exists($object, $setter)) {
            $object->$setter($value);
        }
    }

    public function normalizeCollection($data, $resource_metadata) {
        $normalized = [];

        foreach($data as $item) {
            $normalized[] = $this->normalizeItem($item, $resource_metadata);
        }

        return $normalized;
    }

    public function normalizeItem($data, $resource_metadata) {
        $normalized = [];

        foreach($resource_metadata['fields'] as $field) {
            $getter = 'get' . ucfirst($field['name']);
            $value = $data->$getter();
            
            if(isset($field['normalizer'])) {
                $metadata = $resource_metadata;

                if('relation' == $field['type']) {
                    $metadata = $this->xmlMetadata->getMetadataFor($field['resource']);
                }

                $normalizer = $this->normalizers[$field['normalizer']];
                $value = $normalizer->normalize($field, $value, $metadata, $data);
            } else if('relation_single' == $field['type']) {
                $metadata = $this->xmlMetadata->getMetadataFor($field['resource']);
                $value = $this->normalizers['RestOnPhp\Normalizer\ObjectIdNormalizer']->normalize($field, $value, $metadata, $data);
            } else if('relation_collection' == $field['type']) {
                $metadata = $this->xmlMetadata->getMetadataFor($field['resource']);
                $value = $this->normalizers['RestOnPhp\Normalizer\CollectionIdsNormalizer']->normalize($field, $value, $metadata, $data);
            } else if('datetime' == $field['type'] && $value instanceof DateTime) {
                $value = $value->format($field['datetime-format']);
            } else if(is_object($value)) {
                $value = $this->cast($value->__toString(), $field['type']);
            } else {
                $value = $this->cast($value, $field['type']);
            }
            
            $normalized[$field['name']] = $value;
        }

        return $normalized;
    }

    private function cast($value, $type) {
        if('boolean' == $type) {
            return (bool)$value;
        }

        if('integer' == $type) {
            return (int)$value;
        }

        if('string' == $type) {
            return (string)$value;
        }

        return $value;
    }
}