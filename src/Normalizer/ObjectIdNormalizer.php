<?php
namespace RestOnPhp\Normalizer;

use Doctrine\ORM\EntityManager;
use RestOnPhp\Metadata\XmlMetadata;

/**
 * @property EntityManager $entityManager
 * @property XmlMetadata $xmlMetadata
 */
class ObjectIdNormalizer {
    private $entityManager, $xmlMetadata;

    public function __construct(
        EntityManager $entityManager,
        XmlMetadata $xmlMetadata
    ) {
        $this->xmlMetadata = $xmlMetadata;
        $this->entityManager = $entityManager;
    }

    public function denormalize($field, $value, $resource_metadata) {
        $id_field = $resource_metadata['id'];
        
        if(!$value) {
            return null;
        }

        return $this->entityManager->getRepository($resource_metadata['entity'])->findOneBy([
            $id_field => $value
        ]);
    }

    public function normalize($field, $data, $resource_metadata) {
        if(!$data) {
            return null;
        }

        $id = $resource_metadata['id'];
        $getter = 'get' . ucfirst($id);
        return $data->$getter();
    }
}