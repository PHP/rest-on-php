<?php

declare(strict_types=1);

namespace RestOnPhp\Normalizer;

interface NormalizerInterface {
    function normalize($field, $value, $metadata, $object);
    function denormalize($field, $value, $entity_metadata, $object);
}