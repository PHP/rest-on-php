<?php
namespace RestOnPhp;

use Exception;
use DOMDocument;
use ReflectionClass;
use RuntimeException;
use ReflectionException;
use RestOnPhp\Metadata\XmlMetadata;
use Symfony\Component\Routing\Route;
use RestOnPhp\Event\PreNormalizeEvent;
use RestOnPhp\Event\PreSerializeEvent;
use RestOnPhp\Event\PostNormalizeEvent;
use RestOnPhp\Event\PostSerializeEvent;
use RestOnPhp\Normalizer\RootNormalizer;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouteCollection;
use RestOnPhp\DependencyInjection\Compiler\EventPass;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use RestOnPhp\DependencyInjection\Compiler\LoggerPass;
use RestOnPhp\Handler\Response\HandlerResponseInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Matcher\CompiledUrlMatcher;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\Validator\Exception\ValidatorException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use RestOnPhp\Performance\Instrumentation;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\Routing\Exception\NoConfigurationException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\Dumper\CompiledUrlMatcherDumper;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

class Kernel implements HttpKernelInterface {
    /**
     * @var array $routes
     */
    private $routes;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerBuilder $dependencyContainer
     */
    private $dependencyContainer;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @var \RestOnPhp\Metadata\XmlMetadata
     */
    private $metadata;

    /**
     * @var \Symfony\Component\Routing\RequestContext
     */
    private $context;

    /**
     * @var \Monolog\Logger
     */
    private $logger;

    /**
     * @var string
     */
    private $env;

    /**
     * @var boolean
     */
    private $debug;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var array
     */
    private $routeAttributes;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var Instrumentation
     */
    private $instrumentation;

    public function __construct($env, $debug, $enable_instrumentation = false) {
        $this->instrumentation = new Instrumentation($enable_instrumentation);
        $this->instrumentation->startTransaction('total');

        $this->env = $env;
        $this->debug = $debug;
        $this->parameters = [];

        $this->instrumentation->startTransaction('configuration');

        $this->loadParametersXml();
        $this->instrumentation->setLogDir($this->getLogDir());
        ini_set('error_log', $this->getLogDir() . '/php_error.log');

        $this->checkDirs();
        $this->loadDependencyContainer();
        $this->loadRoutes();

        $this->eventDispatcher = $this->dependencyContainer->get('api.event.dispatcher');
        $this->logger = $this->dependencyContainer->get('api.logger');
        $this->metadata = $this->dependencyContainer->get('api.metadata.xml');

        $this->instrumentation->endTransaction('configuration');
    }

    private function loadParametersXml() {
        $this->instrumentation->startTransaction('parameters_xml_load');
        if(file_exists($this->getConfigDir() . '/parameters.xml.php')) {
            $this->parameters = require_once $this->getConfigDir() . '/parameters.xml.php';
            return;
        }

        $parameters_doc  = new DOMDocument();
        $parameters_doc->load($this->getConfigDir() . '/parameters.xml');

        /**
         * @var DOMElement[]
         */
        $parameters_nodes = $parameters_doc->getElementsByTagName('parameter');

        foreach($parameters_nodes as $parameter_node) {
            $this->parameters[$parameter_node->getAttribute('key')] = $parameter_node->nodeValue;
        }

        if(!isset($this->parameters['cache_dir'])) {
            throw new InvalidConfigurationException('Missing "cache_dir" in parameters.xml');
        }

        if(!isset($this->parameters['log_dir'])) {
            throw new InvalidConfigurationException('Missing "log_dir" in parameters.xml');
        }

        Utils::writeArrayToFile($this->getConfigDir() . '/parameters.xml.php', $this->parameters);
        $this->instrumentation->endTransaction('parameters_xml_load');
    }

    public function checkDirs() {
        if(!is_dir($this->getCacheDir())) {
            mkdir($this->getCacheDir(), 0777, true);
        }

        if(!is_dir($this->getLogDir())) {
            mkdir($this->getLogDir(), 0777, true);
        }
    }

    private function loadDependencyContainer() {
        $this->instrumentation->startTransaction('dependency_container_load');
        $cache_dir = $this->getCacheDir();

        if($this->env != 'cli' && file_exists($cache_dir . '/CompiledDependencyContainer.php')) {
            require_once $cache_dir . '/CompiledDependencyContainer.php';
            $this->dependencyContainer = new \CompiledDependencyContainer();
            $this->instrumentation->endTransaction('dependency_container_load');
            return;
        }

        $this->dependencyContainer = new ContainerBuilder();

        $this->dependencyContainer->setParameter('project_dir', $this->getProjectDir());
        $this->dependencyContainer->setParameter('public_dir', $this->getPublicDir());
        $this->dependencyContainer->setParameter('config_dir', $this->getConfigDir());

        $this->dependencyContainer->addCompilerPass(new EventPass());
        $this->dependencyContainer->addCompilerPass(new LoggerPass());

        $loader = new XmlFileLoader($this->dependencyContainer, new FileLocator($this->getConfigDir()));
        $loader->load('services.xml');
        $this->dependencyContainer->compile();
        $dumper = new PhpDumper($this->dependencyContainer);

        if(!is_dir($cache_dir)) {
            mkdir($cache_dir, 0777, true);
        }

        file_put_contents(
            $cache_dir . '/CompiledDependencyContainer.php',
            $dumper->dump(['class' => 'CompiledDependencyContainer'])
        );

        $this->instrumentation->endTransaction('dependency_container_load');
    }

    private function loadRoutes() {
        $this->instrumentation->startTransaction('routes_load');
        $cache_dir = $this->getCacheDir();

        if(!file_exists($cache_dir . '/routes.php')) {
            /**
             * @var XmlMetadata
             */
            $metadata = $this->dependencyContainer->get('api.metadata.xml');
            $routes = $metadata->getRouteMetadata();
            $routeCollection = new RouteCollection();

            foreach($routes as $route) {

                $routeCollection->add($route['name'], new Route($route['path'], [
                    'resource' => $route['resource'],
                    'handler' => $route['handler'],
                    'method' => $route['method'],
                    'secure' => $route['secure'],
                    'roles' => $route['roles']
                ], [], [], '', [], [ $route['http_method'] ], ''));
            }

            $this->routes = (new CompiledUrlMatcherDumper($routeCollection))->getCompiledRoutes();
            file_put_contents($cache_dir . '/routes.php', sprintf("<?php\nreturn %s;\n", var_export($this->routes, true)));
        } else {
            $this->routes = require_once $cache_dir . '/routes.php';
        }

        $this->instrumentation->endTransaction('routes_load');
    }

    private function normalize($resource_name, HandlerResponseInterface $handler_response) {
        $this->instrumentation->startTransaction('normalization');

        $this->logger->info('NORMALIZE', [
            'resource_name' => $resource_name
        ]);

        $resource_metadata = $this->metadata->getMetadataFor($resource_name);

        if($resource_metadata['normalizer']) {
            $normalizer = $this->dependencyContainer->get($resource_metadata['normalizer']);
        } else {
            /**
             * @var RootNormalizer
             */
            $normalizer = $this->dependencyContainer->get('api.normalizer');
        }

        if($handler_response->getCardinality() == HandlerResponseInterface::CARDINALITY_COLLECTION) {
            $normalized = ['items' => $normalizer->normalizeCollection($handler_response->getData(), $resource_metadata) ];
            $normalized['pagination'] = $handler_response->getPagination();
        } else if($handler_response->getCardinality() == HandlerResponseInterface::CARDINALITY_SINGLE) {
            $normalized = $normalizer->normalizeItem($handler_response->getData(), $resource_metadata);
        } else {
            $normalized = $handler_response->getData();
        }

        $extras = $handler_response->getExtras();

        foreach($extras as $key => $extra) {
            $normalized[$key] = $extra;
        }

        $this->instrumentation->endTransaction('normalization');
        return $normalized;
    }

    private function serialize($data) {
        $this->instrumentation->startTransaction('serialization');
        $this->logger->info('SERIALIZE');
        $serialized = json_encode($data);
        $this->instrumentation->endTransaction('serialization');
        return $serialized;
    }

    private function getHandler() {
        $this->instrumentation->startTransaction('handler_load');
        $this->logger->info('HANDLER_LOAD');
        
        $matcher = new CompiledUrlMatcher($this->routes, $this->context);
        $this->routeAttributes = $matcher->match($this->request->getPathInfo());
        $parameters = [];
        $entityClass = null;
        $handler_id = isset($this->routeAttributes['handler']) ? $this->routeAttributes['handler'] : '';
        $resource_metadata = $this->metadata->getMetadataFor($this->routeAttributes['resource']);
        $entityClass = $resource_metadata['entity'];

        if(!class_exists($entityClass)) {
            $this->logger->error('HANDLER_LOAD_FAIL', [
                'message' => sprintf('%s resource class does not exist!', $entityClass),
                'attributes' => $this->routeAttributes
            ]);

            throw new ResourceNotFoundException(sprintf('%s resource class does not exist!', $entityClass));
        }

        try {
            $handler = $this->dependencyContainer->get($handler_id);
        } catch(Exception $e) {
            throw new NoConfigurationException(sprintf('Unable to instantiate handler "%s" for resource %s', $handler_id, $this->routeAttributes['resource']));
        }

        $method = 'handle';
        
        if(isset($this->routeAttributes['method'])) {
            $method = $this->routeAttributes['method'];
        }

        $reflectionClass = new \ReflectionClass($handler);

        if(!$reflectionClass->implementsInterface(new ReflectionClass('RestOnPhp\Handler\HandlerInterface'))) {
            throw new RuntimeException(sprintf('Handler %s must implement RestOnPhp\Handler\HandlerInterface', $reflectionClass->getName()));
        }

        $filter_ids = $this->metadata->getFilterMetadataFor($this->routeAttributes['resource']);
        $filters = [];

        foreach($filter_ids as $filter_id) {
            $filters[] = $this->dependencyContainer->get($filter_id);
        }

        $handler->setFilters($filters);

        $filler_ids = $this->metadata->getFillerMetadataFor($this->routeAttributes['resource']);
        $fillers = [];

        foreach($filler_ids as $filler_id) {
            $fillers[] = $this->dependencyContainer->get($filler_id);
        }

        $handler->setFillers($fillers);

        try {
            $reflectionMethod = $reflectionClass->getMethod($method);
        } catch(ReflectionException $e) {
            throw new ResourceNotFoundException('Action not implemented');
        }

        $parameters[] = $this->routeAttributes['resource'];

        foreach($reflectionMethod->getParameters() as $parameter) {
            if(isset($this->routeAttributes[$parameter->name])) {
                $parameters[] = $this->routeAttributes[$parameter->name];
            }
        }

        $parameters[] = $entityClass;
        $parameters[] = $resource_metadata;
        $this->instrumentation->endTransaction('handler_load');
        return [ $handler, $reflectionMethod, $parameters];
    }

    private function security($resource_name) {
        $this->instrumentation->startTransaction('security_check');
        $this->logger->info('SECURITY_CHECK');
        /**
         * @var Security\Authorization
         */
        $authorization = $this->dependencyContainer->get('api.security.authorization');
        $authorization->authorize($resource_name, $this->routeAttributes);

        $this->instrumentation->endTransaction('security_check');
    }

    /**
     * @param Request $request
     * @param int $type  The type of the request (one of HttpKernelInterface::MAIN_REQUEST or HttpKernelInterface::SUB_REQUEST)
     * @param bool $catch Whether to catch exceptions or not
     * @return Response
     * @throws \Exception When an Exception occurs during processing
     */
    public function handle(Request $request, int $type = self::MAIN_REQUEST, bool $catch = true): Response {
        $this->instrumentation->startTransaction('handle');
        if(Request::METHOD_OPTIONS == $request->getMethod()) {
            $this->instrumentation->endTransaction('handle');
            return new Response('', 200);
        }

        $this->logger->info('REQUEST', [
            'client_ip' => $request->getClientIp(),
            'method' => $request->getMethod(),
            'uri' => $request->getUri(),
            'query_string' => $request->getQueryString(),
            'content' => $request->getContent()
        ]);

        $this->request = $request;
        $request_stack = $this->dependencyContainer->get('api.request.stack');
        $request_stack->push($this->request);
        $this->context = new RequestContext();
        $this->context->fromRequest($request);

        try {
            list($handler, $reflectionMethod, $args) = $this->getHandler();
            $resource_name = isset($args[0]) ? $args[0] : null;
            $this->security($resource_name);

            /**
             * @var HandlerResponseInterface
             */
            $handler_response = $reflectionMethod->invokeArgs($handler, $args);

            if($handler_response->getData() instanceof Response) {
                $this->instrumentation->endTransaction('handle');
                return $handler_response->getData();
            }

            $this->eventDispatcher->dispatch(new PreNormalizeEvent($resource_name, $handler_response), PreNormalizeEvent::NAME);
            $normalized = $this->normalize($resource_name, $handler_response);
            $this->eventDispatcher->dispatch(new PostNormalizeEvent($resource_name, $handler_response, $normalized), PostNormalizeEvent::NAME);

            $this->eventDispatcher->dispatch(new PreSerializeEvent($resource_name, $handler_response, $normalized), PreSerializeEvent::NAME);
            $serialized = $this->serialize($normalized);
            $this->eventDispatcher->dispatch(new PostSerializeEvent($resource_name, $handler_response, $normalized, $serialized), PostSerializeEvent::NAME);

            $response = new Response($serialized, 200, [ 'Content-Type' => 'application/json' ]);
        } catch(UniqueConstraintViolationException $e) {
            $response = new Response(
                json_encode([
                    'message' => 'Item already exists!',
                    'exception_message' => $e->getMessage()
                ]), 409, ['Content-Type' => 'application/json']
            );
        } catch(NoConfigurationException $e) { 
            $response = new Response(json_encode([
                'message' => 'Not found!',
                'exception_message' => $e->getMessage()
            ]), Response::HTTP_NOT_FOUND, ['Content-Type' => 'application/json']);
        } catch (ResourceNotFoundException $e) {
		    $response = new Response(json_encode([
                'message' => 'Not found!',
                'exception_message' => $e->getMessage()
            ]), Response::HTTP_NOT_FOUND, ['Content-Type' => 'application/json']);
		} catch (MethodNotAllowedException $e) {
            $response = new Response(json_encode([
                'message' => 'Not allowed!',
                'exception_message' => $e->getMessage()
            ]), Response::HTTP_METHOD_NOT_ALLOWED, ['Content-Type' => 'application/json']);
        } catch(HttpException $e) {
            $response = new Response(json_encode([
                'message' => $e->getMessage(),
                'exception_message' => 'HTTP Exception'
            ]), $e->getStatusCode(), ['Content-Type' => 'application/json']);
        }

        $this->logger->info('RESPONSE', [
            'content_length' => strlen($response->getContent()),
            'status_code' => $response->getStatusCode(),
            'content_type' => $response->headers->get('Content-Type')
        ]);

        $this->instrumentation->endTransaction('handle');
        $this->instrumentation->endTransaction('total');
        return $response;
    }

    public function getDependencyContainer() {
        return $this->dependencyContainer;
    }

    public function getCacheDir() {
        if(isset($this->parameters['cache_dir'])) {
            return $this->parameters['cache_dir'];
        }

        return $this->getProjectDir() . '/cache';
    }

    public function getLogDir() {
        if(isset($this->parameters['log_dir'])) {
            return $this->parameters['log_dir'];
        }

        return $this->getProjectDir() . '/log';
    }

    public function getConfigDir() {
        return $this->getProjectDir() . '/config';
    }

    public function getPublicDir() {
        return $this->getProjectDir() . '/web';
    }

    public function getProjectDir() {
        return __DIR__ . '/../../../..';
    }
}
