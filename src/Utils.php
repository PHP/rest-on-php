<?php
namespace RestOnPhp;

use Exception;
use RestOnPhp\Exception\CommandException;

class Utils {
    public static function camelize($string) {
        return preg_replace_callback('/_([a-z])/', 
            function($matches) {
                return strtoupper($matches[1]);
            }, $string);
    }

    public static function snake_case($string) {
        $string = lcfirst($string);
        $string = str_replace(' ', '_', $string);
        
        return preg_replace_callback('/[A-Z]/', 
            function($matches) {
                return '_' . strtolower($matches[1]);
            }, $string);
    }

    public static function scandir($dir, $callback, $recursive = false) {
        $files = scandir($dir);
        
        foreach($files as $file) {
            if($file == '.' || $file == '..') {
                continue;
            }

            if(is_dir($dir . DIRECTORY_SEPARATOR . $file) && $recursive) {
                self::scandir($dir . DIRECTORY_SEPARATOR . $file, $callback, $recursive);
            }

            $callback($dir, $file);
        }
    }

    public static function executeCommand($command) {
        $output = [];
        $exit_code = 0;
        exec($command, $output, $exit_code);

        if(0 != $exit_code) {
            throw new CommandException(sprintf('%s returned %d', $command, $exit_code), implode('', $output), $exit_code);
        }

        return [ $exit_code, $output ];
    }

    public static function randomString($length) {
        $pool = str_shuffle('0123456789abcdefghijklmnopqrtsuvwxyzABCDEFGHIJKLMNOPQRTSUVWXYZ');
        $value = '';

        for ($i = 0; $i < $length; $i++) {
            $pos = rand(0, strlen($pool) - 1);
            $value .= $pool[$pos];
        }

        return $value;
    }

    public static function distributedPath() {
        $pool = str_shuffle('0123456789abcdef');
        $l1 = '';
        $l2 = '';

        for ($i = 0; $i < 2; $i++) {
            $pos = rand(0, strlen($pool) - 1);
            $l1 .= $pool[$pos];
        }

        for ($i = 0; $i < 2; $i++) {
            $pos = rand(0, strlen($pool) - 1);
            $l2 .= $pool[$pos];
        }

        return sprintf('%s/%s', $l1, $l2);
    }

    public static function writeArrayToFile($file, $array) {
        $dir = dirname($file);

        if(!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        file_put_contents($file, "<?php\nreturn " . var_export($array, true) . ";\n");
    }
}