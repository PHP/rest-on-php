<?php
namespace RestOnPhp\Metadata;

use RestOnPhp\Utils;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Config\Util\Exception\InvalidXmlException;

class XmlMetadata {
    private $xpath;
    private $metadata;
    private $cache_dir;
    private $entityIndex;
    private $metadata_path;
    private $route_metadata;

    public function __construct(string $metadata_path, string $cache_dir = null) {
        $this->metadata = [];
        $this->entityIndex = [];
        $this->metadata_path = $metadata_path;
        $this->cache_dir = $cache_dir . '/resource_mappings';

        if(!is_dir($this->cache_dir)) {
            mkdir($this->cache_dir, 0777, true);
        }

        if(!file_exists($this->cache_dir . '/routes.xml.php')) {
            $this->loadMetadata();
        } else {
            $this->route_metadata = require_once $this->cache_dir . '/routes.xml.php';
        }
    }

    private function loadMetadata() {
        $finder = new Finder();

        $finder
            ->files()
            ->in($this->metadata_path)
            ->name('*.xml');

        foreach($finder as $file) {
            $this->loadResource($file->getPathname());
        }
    }

    private function loadResource($metadata_file) {
        $metadata_doc = new \DOMDocument();
        $metadata_doc->preserveWhiteSpace = false;
        $metadata_doc->load($metadata_file);
        
        if(!@$metadata_doc->schemaValidate(__DIR__ . '/../../config/api-resource.xsd')) {
            throw new InvalidXmlException($metadata_file . ' is not a valid XML document.');
        }

        $this->xpath = new \DOMXPath($metadata_doc);
        $this->xpath->registerNamespace('ns', 'urn:mapping');

        /**
         * @var DOMElement[]
         */
        $resources = $this->xpath->query('//ns:mapping/ns:resource');
        
        foreach($resources as $resource) {
            $name = $resource->getAttribute('name');
            $entity = $resource->getAttribute('entity');
            $id = $resource->getAttribute('id');
            $normalizer = $resource->getAttribute('normalizer') ? $resource->getAttribute('normalizer') : null;
            $denormalizer = $resource->getAttribute('denormalizer') ? $resource->getAttribute('denormalizer') : null;
            $routes = [];
            $fields = [];
            $filters = [];
            $fillers = [];

            foreach($resource->getElementsByTagName('route') as $route_element) {
                $route_name = $route_element->getAttribute('name');
                $route_http_method = $route_element->getAttribute('http_method');
                $route_handler = $route_element->getAttribute('handler');
                $route_method = $route_element->getAttribute('method');
                $route_path = $route_element->getAttribute('path');
                $secure = $route_element->getAttribute('secure');
                $secure = $secure == 'true' ? true : false;
                $roles = $route_element->getAttribute('roles');
                $roles = $roles ? explode('|', $roles) : [];

                $routes[$route_name] = [
                    'name' => $route_name,
                    'http_method' => $route_http_method,
                    'handler' => $route_handler,
                    'method' => $route_method,
                    'path' => $route_path,
                    'secure' => $secure,
                    'roles' => $roles
                ];

                $this->route_metadata[] = [
                    'resource' => $name,
                    'name' => $name . '_' . $route_name,
                    'path' => $route_path,
                    'method' => $route_method,
                    'http_method' => $route_http_method,
                    'handler' => $route_handler,
                    'secure' => $secure,
                    'roles' => $roles
                ];
            }

            foreach($resource->getElementsByTagName('field') as $field_element) {
                $field_name = $field_element->getAttribute('name');
                $field_type = $field_element->getAttribute('type');
                
                $field_normalizer = $field_element->getAttribute('normalizer') ? $field_element->getAttribute('normalizer') : null;
                $field_denormalizer = $field_element->getAttribute('denormalizer') ? $field_element->getAttribute('denormalizer') : null;
                
                $field_filter_type = $field_element->getAttribute('filter-type');
                $field_filter_type = $field_filter_type ? $field_filter_type : 'exact';

                $filter_related_field = $field_element->getAttribute('filter-related-field');
                $filter_related_field = $filter_related_field ? $filter_related_field : 'id';

                $field_readonly = $field_element->getAttribute('readonly');
                $field_readonly = $field_readonly == 'true' ? true : false;

                $field_resource = $field_element->getAttribute('resource');
                $field_resource = $field_resource ? $field_resource : null;

                $field_datetime_format = $field_element->getAttribute('datetime-format');
                $field_datetime_format = $field_datetime_format ? $field_datetime_format : 'Y-m-d H:i:s';

                $fields[$field_name] = [
                    'name' => $field_name,
                    'type' => $field_type,
                    'resource' => $field_resource,
                    'filter-type' => $field_filter_type,
                    'filter-related-field' => $filter_related_field,
                    'normalizer' => $field_normalizer,
                    'denormalizer' => $field_denormalizer,
                    'readonly' => $field_readonly,
                    'datetime-format' => $field_datetime_format,
                ];
            }

            foreach($resource->getElementsByTagName('filter') as $field_element) {
                $filters[] = $field_element->getAttribute('id');
            }

            foreach($resource->getElementsByTagName('filler') as $field_element) {
                $fillers[] = $field_element->getAttribute('id');
            }

            $this->metadata[$name] = [
                'name' => $name,
                'entity' => $entity,
                'id' => $id,
                'normalizer' => $normalizer,
                'denormalizer' => $denormalizer,
                'routes' => $routes,
                'fields' => $fields,
                'filters' => $filters,
                'fillers' => $fillers
            ];

            $this->entityIndex[$entity] = $name;

            Utils::writeArrayToFile($this->cache_dir . '/' . $name . '.xml.php', $this->metadata[$name]);
        }

        Utils::writeArrayToFile($this->cache_dir . '/routes.xml.php', $this->route_metadata);
    }

    public function getNormalizerFieldsFor(string $name) {
        $resource = null;
        $fields = [];

        $resource = $this->getMetadataFor($name);

        foreach($resource['fields'] as $field) {
            $fields[] = $field['name'];
        }

        return $fields;
    }

    public function getMetadataForDocs() {
        $docs = [];

        foreach($this->metadata as $resource) {
            $routes = [];
            $fields = [];

            foreach($resource['routes'] as $route) {
                $routes[] = $route;
            }

            foreach($resource['fields'] as $field) {
                $fields[] = $field;
            }

            $docs[] = [
                'name' => $resource['name'],
                'entity' => $resource['entity'],
                'id' => $resource['id'],
                'fields' => $fields,
                'routes' => $routes
            ];
        }

        return $docs;
    }

    public function getMetadataFor($name) {
        if(!isset($this->metadata[$name]) && file_exists($this->cache_dir . '/' . $name . '.xml.php')) {
            $this->metadata[$name] = require_once $this->cache_dir . '/' . $name . '.xml.php';
        }
        
        return $this->metadata[$name];
    }

    public function getFieldMetadataFor($name, $field_name) {
        $resource = $this->getMetadataFor($name);
        return isset($resource['fields'][$field_name]) ? $resource['fields'][$field_name] : null;
    }

    public function getRouteMetadataFor($name, $route_name) {
        $resource = $this->getMetadataFor($name);
        return $resource['routes'][$route_name];
    }

    public function getRouteMetadata() {
        return $this->route_metadata;
    }

    public function getIdFieldNameFor($name) {
        $resource = $this->getMetadataFor($name);
        return $resource['id'];
    }

    public function getFilterMetadataFor($name) {
        $resource = $this->getMetadataFor($name);
        return $resource['filters'];
    }

    public function getFillerMetadataFor($name) {
        $resource = $this->getMetadataFor($name);
        return $resource['fillers'];
    }
}
