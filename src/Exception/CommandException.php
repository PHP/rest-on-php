<?php
declare(strict_types=1);

namespace RestOnPhp\Exception;

use Exception;

class CommandException extends Exception {
    private $output;

    public function __construct($message, $output, $return_code = 1, $previous = null) {
        $this->output = $output;
        parent::__construct($message, $return_code, $previous);
    }

    public function getOutput() {
        return $this->output;
    }
}