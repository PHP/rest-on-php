<?php
namespace RestOnPhp\Security;

interface SecureUser {
    /**
     * Get username
     * @return string
     */
    function getUsername(): string;

    /**
     * @param string
     * @return SecureUser
     */
    function setUsername(string $username): SecureUser;

    /**
     * @return string
     */
    function getPassword(): string;

    /**
     * @param string
     * @return SecureUser
     */
    function setPassword(string $password): SecureUser;

    /**
     * @param string $role
     * @return bool
     */
    function hasRole(string $role): bool;

    /**
     * @return bool
     */
    function isSuperAdmin(): bool;

    /**
     * @param string $token
     * @return SecureUser
     */
    function setToken(string $token): SecureUser;

    /**
     * @return string
     */
    function getToken(): string;

    /**
     * @return array
     */
    function getRoles(): array;
}