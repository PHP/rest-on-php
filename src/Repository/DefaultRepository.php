<?php
namespace RestOnPhp\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DefaultRepository extends EntityRepository {

    public function get($filters = [], $pagination = [], $order = [], $single = false) {
        $q = $this->createQueryBuilder('r');

        foreach($order as $field => $direction) {
            $q->addOrderBy('r.' . $field, $direction);
        }

        foreach($filters['relation'] as $field => $definition) {
            $q->innerJoin('r.' . $field, $field);
            $q->andWhere(sprintf('%s.%s = :%s', $field, $definition['metadata']['filter-related-field'], $field));
            $q->setParameter($field, $definition['value']);
        }

        foreach($filters['partial'] as $field => $definition) {
            $q->andWhere(sprintf('r.%s LIKE :%s', $field, $field));
            $q->setParameter($field, '%' . $definition['value'] . '%');
        }

        foreach($filters['exact'] as $field => $definition) {
            if(is_array($definition['value'])) {
                $q->andWhere(sprintf('r.%s IN (:%s)', $field, $field));
                $q->setParameter($field, $definition['value']);
            } else {
                $q->andWhere(sprintf('r.%s = :%s', $field, $field));
                $q->setParameter($field, $definition['value']);
            }
        }

        foreach($filters['gt'] as $field => $definition) {
            $q->andWhere(sprintf('r.%s > :%s', $field, $field));
            $q->setParameter($field, $definition['value']);
        }

        foreach($filters['lt'] as $field => $definition) {
            $q->andWhere(sprintf('r.%s < :%s', $field, $field));
            $q->setParameter($field, $definition['value']);
        }

        foreach($filters['gte'] as $field => $definition) {
            $q->andWhere(sprintf('r.%s >= :%s', $field, $field));
            $q->setParameter($field, $definition['value']);
        }

        foreach($filters['lte'] as $field => $definition) {
            $q->andWhere(sprintf('r.%s <= :%s', $field, $field));
            $q->setParameter($field, $definition['value']);
        }

        foreach($filters['default'] as $filter) {
            $q = $filter->filter($q);
        }

        if($pagination && is_array($pagination) && isset($pagination['page'], $pagination['per_page'])) {
            $q->setMaxResults($pagination['per_page']);
            $q->setFirstResult(($pagination['page'] - 1) * $pagination['per_page']);
        }

        if($single) {
            return $q->getQuery()->getOneOrNullResult();
        }

        return new Paginator($q->getQuery());
    }
}
