<?php
declare(strict_types=1);

namespace RestOnPhp\Performance;

class Measurement {
    private $end_time;
    private $start_time;
    private $execution_time;
    private $transaction_id;
    private $transaction_name;

    public function __construct($transaction_name) {
        $this->end_time = 0;
        $this->start_time = 0;
        $this->execution_time = 0;
        $this->transaction_name = $transaction_name;
        $this->transaction_id = str_replace('.', '', uniqid('', true));
    }

    public function start(): self {
        $this->start_time = microtime(true);
        return $this;
    }

    public function stop(): self {
        $this->end_time = microtime(true);
        $this->execution_time = $this->end_time - $this->start_time;
        return $this;
    }

    public function getStartTime(): float {
        return $this->start_time;
    }

    public function getEndTime(): float {
        return $this->end_time;
    }

    public function getExecutionTime(): float {
        return $this->execution_time;
    }

    public function getTransactionId(): string {
        return $this->transaction_id;
    }

    public function getTransactionName(): string {
        return $this->transaction_name;
    }
}