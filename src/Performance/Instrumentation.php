<?php
declare(strict_types=1);

namespace RestOnPhp\Performance;

use Exception;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;

/**
 * @property Measurement[] $measurements
 */
class Instrumentation {
    private $enabled;
    private $log_dir;
    private $measurements;

    public function __construct($enabled = false) {
        $this->measurements = [];
        $this->enabled = $enabled;

        register_shutdown_function([ $this, 'flush' ]);
    }

    public function setLogDir($log_dir) {
        $this->log_dir = $log_dir;
    }

    public function startTransaction($name): ?Measurement {
        if(!$this->enabled) {
            return null;
        }

        if(isset($this->measurements[$name])) {
            throw new Exception(sprintf('Measurement transaction %s already started', $name));
        }

        $this->measurements[$name] = new Measurement($name);
        $this->measurements[$name]->start();
        return $this->measurements[$name];
    }

    public function endTransaction($name): ?Measurement {
        if(!$this->enabled) {
            return null;
        }

        if(!isset($this->measurements[$name])) {
            throw new Exception(sprintf('Measurement transaction %s not started', $name));
        }

        $this->measurements[$name]->stop();
        return $this->measurements[$name];
    }

    public function flush() {
        if(!$this->enabled) {
            return;
        }

        $logger = new Logger('instrumentation');
        $streamHandler = new StreamHandler($this->log_dir . '/instrumentation.log', Level::Debug);
        $streamHandler->setFormatter(new JsonFormatter());
        $logger->pushHandler($streamHandler);

        foreach($this->measurements as $name => $measurement) {
            $logger->debug($measurement->getTransactionId(), [
                'request_uri' => $_SERVER['REQUEST_URI'],
                'query_string' => $_SERVER['QUERY_STRING'],
                'transactionId' => $measurement->getTransactionId(),
                'transactionName' => $measurement->getTransactionName(),
                'startTime' => $measurement->getStartTime(),
                'endTime' => $measurement->getEndTime(),
                'executionTime' => $measurement->getExecutionTime(),
            ]);
        }
    }
}
