<?php
namespace RestOnPhp\Command;

use Doctrine\ORM\EntityManager;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateUserCommand extends Command {
    protected static $defaultName = 'api:create-user';
    private $entityManager;
    private $user;

    /**
     * @return void
     */
    protected function configure() {
        $this->setName(self::$defaultName);
        $this->addArgument('username', InputArgument::REQUIRED, 'The username of the user.');
        $this->addArgument('password', InputArgument::REQUIRED, 'The password of the user.');
        $this->addOption('do-hash', 'dh', InputOption::VALUE_NONE, 'Should the script hash the password. If hashing is implemeted inside of a User model omit this option.');
        $this->addArgument('roles', InputArgument::REQUIRED, 'User roles');
        
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if(!$this->user) {
            throw new RuntimeException('User object could not be instantiated. Check user entity class.');
        }

        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $roles = explode(',', $input->getArgument('roles'));
        
        if($input->getOption('do-hash')) {
            $password = password_hash($password, PASSWORD_BCRYPT, [
                'cost' => 15
            ]);
        }

        $this->user->setUsername($username);
        $this->user->setPassword($password);
        $this->user->setRoles($roles);
        $this->entityManager->persist($this->user);
        $this->entityManager->flush();

        return 0;
    }

    public function setEntityManager(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function setUserEntity(string $entity) {
        if(!class_exists($entity)) {
            return;
        };

        $this->user = new $entity();
    }
}
