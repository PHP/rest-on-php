<?php
namespace RestOnPhp\Command;

use RuntimeException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class EditUserCommand extends Command {
    protected static $defaultName = 'api:edit-user';
    private $entityManager;
    private $userEntity;

    /**
     * @return void
     */
    protected function configure() {
        $this->setName(self::$defaultName);
        $this->addArgument('username', InputArgument::REQUIRED, 'The username of the user.');
        $this->addArgument('password', InputArgument::REQUIRED, 'The password of the user.');
        $this->addArgument('roles', InputArgument::REQUIRED, 'User roles');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $username = $input->getArgument('username');

        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy([
            'username' => $username
        ]);

        if(!$user) {
            throw new RuntimeException('User object could not be instantiated. Check user entity class.');
        }

        $password = $input->getArgument('password');
        $roles = explode(',', $input->getArgument('roles'));

        $user->setUsername($username);
        $user->setPassword($password);
        $user->setRoles($roles);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return 0;
    }

    public function setEntityManager(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function setUserEntity(string $entity) {
        if(!class_exists($entity)) {
            return;
        };

        $this->userEntity = $entity;
    }
}
